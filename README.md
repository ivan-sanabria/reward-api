# Kaffeeland Reward API - Co Founder Project

version 1.0.0 - 29/06/2019

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/kaffeeland-reward-api.svg)](http://bitbucket.org/ivan-sanabria/kaffeeland-reward-api/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/kaffeeland-reward-api.svg)](http://bitbucket.org/ivan-sanabria/kaffeeland-reward-api/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-reward-api&metric=alert_status)](https://sonarcloud.io/component_measures/metric/alert_status/list?id=ivan-sanabria_kaffeeland-reward-api)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-reward-api&metric=bugs)](https://sonarcloud.io/component_measures/metric/bugs/list?id=ivan-sanabria_kaffeeland-reward-api)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-reward-api&metric=coverage)](https://sonarcloud.io/component_measures/metric/coverage/list?id=ivan-sanabria_kaffeeland-reward-api)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-reward-api&metric=ncloc)](https://sonarcloud.io/component_measures/metric/ncloc/list?id=ivan-sanabria_kaffeeland-reward-api)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-reward-api&metric=sqale_index)](https://sonarcloud.io/component_measures/metric/sqale_index/list?id=ivan-sanabria_kaffeeland-reward-api)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_kaffeeland-reward-api&metric=vulnerabilities)](https://sonarcloud.io/component_measures/metric/vulnerabilities/list?id=ivan-sanabria_kaffeeland-reward-api)

## Specifications

Many customers are loyal to the shops are located in their areas. They would like to get rewarded after consuming x amount
of products. The current status is many shops doesn't know how to reward their users. The current project covers the 
following uses cases:

1. Create rewards for specific application.
2. Update rewards for specific application.
3. Get all rewards for specific application.
4. Get available rewards for specific application.
5. Get reward by identifier for specific application.
6. Delete reward for specific application.

## Requirements

- JDK 11.x
- Kotlin 1.3.40
- Gradle 4.10
- IDE for Kotlin (IntelliJ).

## Running Application inside IDE

To run the application on your IDE:

1. Verify the version of your JDK - 11.x or higher.
2. Verify the version of Kotlin Plugin - 1.3.40 or higher.
3. Download the source code from repository.
4. Integrate your project with IDE:
    - [IntelliJ](https://www.jetbrains.com/help/idea/gradle.html)
5. Open IDE and run the class **Application.kt**.

## Running Unit Tests on Terminal

To run the unit tests on terminal:

1. Verify the version of your JDK - 11.x or higher.
2. Download the source code from repository.
3. Open a terminal.
4. Go to the root location of the source code.
5. Execute the commands:

```bash
    ./gradlew clean test
```

## Check Application Test Coverage on Terminal using Jacoco

To check test coverage on terminal with jacoco:

1. Verify the version of your JDK - 11.x or higher.
2. Download the source code from repository.
3. Open a terminal.
4. Go to the root location of the source code.
5. Execute the commands:

```bash
    ./gradlew clean build
    open build/jacoco/html/index.html
```

## API Documentation

To see swagger documentation:

1. Open a browser
2. Go to http://${CNAME_PREFIX}.${AWS_DEFAULT_REGION}.elasticbeanstalk.com /swagger-ui.html

# Contact Information

Email: icsanabriar@googlemail.com