/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.reward.controller

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.kaffeeland.reward.Application
import com.kaffeeland.reward.dto.RewardDto
import com.kaffeeland.reward.service.RewardService
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.time.LocalDate

/**
 * Class to handle test cases for client rewards on controller layer.
 *
 * @author  Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since   1.0.0
 */
@ContextConfiguration(classes = [Application::class])
@RunWith(SpringRunner::class)
@WebMvcTest(RewardController::class)
class RewardControllerTest {

    /**
     * Mock reward service.
     */
    @MockBean
    private lateinit var rewardService: RewardService

    /**
     * Define the object mappers used to transform dto to JSON.
     */
    @Autowired
    private lateinit var mapper: ObjectMapper

    /**
     * Mock the web application.
     */
    @Autowired
    private lateinit var mockMvc: MockMvc


    @Test
    fun `POST request to create a reward successfully`() {

        val application = "application-1a"
        val rewardDto = RewardDto("", 1, LocalDate.now(), "Test Reward", "Test description")
        val expectedBody = ""

        given(rewardService.createReward(rewardDto, application))
            .willReturn("12345")

        val content = mapper.writeValueAsString(rewardDto)

        val result = mockMvc.perform(
            MockMvcRequestBuilders.post("/$REWARD_ENDPOINT")
                .header(APPLICATION_HEADER, application)
                .content(content)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
            .andExpect(MockMvcResultMatchers.status().isCreated)
            .andReturn()

        val body = result.response.contentAsString
        assertEquals(expectedBody, body)
    }

    @Test
    fun `POST request to create a reward with service failure`() {

        val application = "application-1a"
        val rewardDto = RewardDto("", 1, LocalDate.now(), "Test Reward", "Test description")
        val expectedBody = ""

        given(rewardService.createReward(rewardDto, application))
            .willReturn("")

        val content = mapper.writeValueAsString(rewardDto)

        val result = mockMvc.perform(
            MockMvcRequestBuilders.post("/$REWARD_ENDPOINT")
                .header(APPLICATION_HEADER, application)
                .content(content)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
            .andExpect(MockMvcResultMatchers.status().isInternalServerError)
            .andReturn()

        val body = result.response.contentAsString
        assertEquals(expectedBody, body)
    }

    @Test
    fun `POST request to create a reward with client failure`() {

        val application = "application-1a"
        val content = "Hello, this is not a json"
        val expectedBody = ""

        val result = mockMvc.perform(
            MockMvcRequestBuilders.post("/$REWARD_ENDPOINT")
                .header(APPLICATION_HEADER, application)
                .content(content)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
            .andExpect(MockMvcResultMatchers.status().isBadRequest)
            .andReturn()

        val body = result.response.contentAsString
        assertEquals(expectedBody, body)
    }

    @Test
    fun `PUT request to update a reward successfully`() {

        val application = "application-1a"
        val identifier = "1234567890"
        val rewardDto = RewardDto(identifier, 1, LocalDate.now(), "Test Reward", "Test description")
        val expectedBody = ""


        given(rewardService.updateReward(identifier, rewardDto, application))
            .willReturn(rewardDto)

        val content = mapper.writeValueAsString(rewardDto)

        val result = mockMvc.perform(
            MockMvcRequestBuilders.put("/$REWARD_ENDPOINT/$identifier")
                .header(APPLICATION_HEADER, application)
                .content(content)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andReturn()

        val body = result.response.contentAsString
        assertEquals(expectedBody, body)
    }

    @Test
    fun `PUT request to update a reward with service error`() {

        val application = "application-1a"
        val identifier = "1234567890"
        val rewardDto = RewardDto(identifier, 1, LocalDate.now(), "Test Reward", "Test description")
        val expectedBody = ""


        given(rewardService.updateReward(identifier, rewardDto, application))
            .willReturn(null)

        val content = mapper.writeValueAsString(rewardDto)

        val result = mockMvc.perform(
            MockMvcRequestBuilders.put("/$REWARD_ENDPOINT/$identifier")
                .header(APPLICATION_HEADER, application)
                .content(content)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
            .andExpect(MockMvcResultMatchers.status().isInternalServerError)
            .andReturn()

        val body = result.response.contentAsString
        assertEquals(expectedBody, body)
    }

    @Test
    fun `PUT request to update a reward with client error`() {

        val application = "application-1a"
        val identifier = "1234567890"
        val expectedBody = ""

        val content = "Hello, this is not a json"

        val result = mockMvc.perform(
            MockMvcRequestBuilders.put("/$REWARD_ENDPOINT/$identifier")
                .header(APPLICATION_HEADER, application)
                .content(content)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
            .andExpect(MockMvcResultMatchers.status().isBadRequest)
            .andReturn()

        val body = result.response.contentAsString
        assertEquals(expectedBody, body)
    }

    @Test
    fun `DELETE request to delete a reward successfully`() {

        val application = "application-1a"
        val identifier = "1234567890"
        val expectedBody = ""

        given(rewardService.deleteReward(identifier, application))
            .willReturn(identifier)

        val result = mockMvc.perform(
            MockMvcRequestBuilders.delete("/$REWARD_ENDPOINT/$identifier")
                .header(APPLICATION_HEADER, application)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andReturn()

        val body = result.response.contentAsString
        assertEquals(expectedBody, body)
    }

    @Test
    fun `DELETE request to delete a reward with service error`() {

        val application = "application-1a"
        val identifier = "1234567890"
        val expectedBody = ""

        given(rewardService.deleteReward(identifier, application))
            .willReturn(null)

        val result = mockMvc.perform(
            MockMvcRequestBuilders.delete("/$REWARD_ENDPOINT/$identifier")
                .header(APPLICATION_HEADER, application)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
            .andExpect(MockMvcResultMatchers.status().isNotFound)
            .andReturn()

        val body = result.response.contentAsString
        assertEquals(expectedBody, body)
    }

    @Test
    fun `DELETE request to delete a reward with client error`() {

        val application = "application-1a"
        val identifier = ""
        val expectedBody = ""

        val result = mockMvc.perform(
            MockMvcRequestBuilders.delete("/$REWARD_ENDPOINT/$identifier")
                .header(APPLICATION_HEADER, application)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
            .andExpect(MockMvcResultMatchers.status().isMethodNotAllowed)
            .andReturn()

        val body = result.response.contentAsString
        assertEquals(expectedBody, body)
    }

    @Test
    fun `GET request to find a reward by id successfully`() {

        val application = "application-1a"
        val identifier = "1234567890"
        val expectedDto = RewardDto(identifier, 1, LocalDate.now(), "Test Reward", "Test description")

        given(rewardService.findRewardById(identifier, application))
            .willReturn(expectedDto)

        val result = mockMvc.perform(
            MockMvcRequestBuilders.get("/$REWARD_ENDPOINT/$identifier")
                .header(APPLICATION_HEADER, application)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andReturn()

        val body = result.response.contentAsString
        val responseDto = mapper.readValue(body, RewardDto::class.java)
        assertEquals(expectedDto, responseDto)
    }

    @Test
    fun `GET request to find a reward by id with service error`() {

        val application = "application-1a"
        val identifier = "1234567890"
        val expectedBody = ""

        given(rewardService.findRewardById(identifier, application))
            .willReturn(null)

        val result = mockMvc.perform(
            MockMvcRequestBuilders.get("/$REWARD_ENDPOINT/$identifier")
                .header(APPLICATION_HEADER, application)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
            .andExpect(MockMvcResultMatchers.status().isNotFound)
            .andReturn()

        val body = result.response.contentAsString
        assertEquals(expectedBody, body)
    }

    @Test
    fun `GET request to list stored rewards successfully`() {

        val application = "application-1a"
        val rewardDto1 = RewardDto("12345", 1, LocalDate.now(), "Test Reward 1", "Test description 1")
        val rewardDto2 = RewardDto("23456", 2, LocalDate.now(), "Test Reward 2", "Test description 2")
        val expectedDtoList = listOf(rewardDto1, rewardDto2)

        given(rewardService.listRewards(0, application))
            .willReturn(expectedDtoList)

        val result = mockMvc.perform(
            MockMvcRequestBuilders.get("/$REWARD_ENDPOINT")
                .header(APPLICATION_HEADER, application)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andReturn()

        val body = result.response.contentAsString
        val responseDtoList: List<RewardDto> = mapper.readValue(body, object : TypeReference<List<RewardDto>>() {})
        assertEquals(expectedDtoList, responseDtoList)
    }

    @Test
    fun `GET request to find available rewards`() {

        val application = "application-1a"
        val rewardDto1 = RewardDto("12345", 1, LocalDate.now(), "Test Reward 1", "Test description 1")
        val rewardDto2 = RewardDto("23456", 2, LocalDate.now(), "Test Reward 2", "Test description 2")
        val expectedDtoList = listOf(rewardDto1, rewardDto2)

        given(rewardService.findAvailableRewards(application))
            .willReturn(expectedDtoList)

        val result = mockMvc.perform(
            MockMvcRequestBuilders.get("/$REWARD_ENDPOINT/available")
                .header(APPLICATION_HEADER, application)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        )
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andReturn()

        val body = result.response.contentAsString
        val responseDtoList: List<RewardDto> = mapper.readValue(body, object : TypeReference<List<RewardDto>>() {})
        assertEquals(expectedDtoList, responseDtoList)
    }

}
