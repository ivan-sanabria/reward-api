/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.reward.service

import com.kaffeeland.reward.domain.Reward
import com.kaffeeland.reward.domain.RewardId
import com.kaffeeland.reward.dto.RewardDto
import com.kaffeeland.reward.repository.RewardRepository
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.doNothing
import org.mockito.BDDMockito.eq
import org.mockito.BDDMockito.given
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.test.context.junit4.SpringRunner
import java.time.LocalDate
import java.util.Optional

/**
 * Class to handle test cases for client rewards on service layer.
 *
 * @author  Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since   1.0.0
 */
@RunWith(SpringRunner::class)
class RewardServiceTest {

    /**
     * Mock reward repository.
     */
    @MockBean
    private lateinit var rewardRepository: RewardRepository

    /**
     * Mock of Reward Service.
     */
    private lateinit var rewardService: RewardService

    @Before
    fun initBean() {
        rewardService = RewardService(rewardRepository)
    }


    @Test
    fun `create a reward successfully with expiration`() {

        val expectedIdentifier = "12345"
        val expectedApplication = "application-1a"

        val rewardDto = RewardDto(
            null,
            1,
            LocalDate.now(),
            "Test Reward",
            "Test Description"
        )

        val rewardId = RewardId(
            rewardDto.id,
            expectedApplication
        )

        val transformReward = Reward(
            rewardId,
            rewardDto.expiration?.toEpochDay(),
            rewardDto.title,
            rewardDto.description,
            rewardDto.available
        )

        val expectedRewardId = RewardId(
            expectedIdentifier,
            expectedApplication
        )

        val saveReward = Reward(
            expectedRewardId,
            rewardDto.expiration?.toEpochDay(),
            rewardDto.title,
            rewardDto.description,
            rewardDto.available
        )

        given(rewardRepository.save(transformReward))
            .willReturn(saveReward)

        val identifier = rewardService.createReward(rewardDto, expectedApplication)
        assertEquals(expectedIdentifier, identifier)
    }

    @Test
    fun `create a reward successfully without expiration`() {

        val expectedIdentifier = "12345"
        val expectedApplication = "application-1a"

        val rewardDto = RewardDto(
            null,
            1,
            null,
            "Test Reward",
            "Test Description"
        )

        val rewardId = RewardId(
            rewardDto.id,
            expectedApplication
        )

        val transformReward = Reward(
            rewardId,
            rewardDto.expiration?.toEpochDay(),
            rewardDto.title,
            rewardDto.description,
            rewardDto.available
        )

        val expectedRewardId = RewardId(
            expectedIdentifier,
            expectedApplication
        )

        val saveReward = Reward(
            expectedRewardId,
            rewardDto.expiration?.toEpochDay(),
            rewardDto.title,
            rewardDto.description,
            rewardDto.available
        )

        given(rewardRepository.save(transformReward))
            .willReturn(saveReward)

        val identifier = rewardService.createReward(rewardDto, expectedApplication)
        assertEquals(expectedIdentifier, identifier)
    }

    @Test
    fun `update a reward successfully with all new attributes except identifier and application`() {

        val identifier = "12345"
        val application = "application-1a"

        val expectedRewardDto = RewardDto(
            identifier,
            1,
            LocalDate.now().plusDays(1),
            "Test Reward",
            "Test Description"
        )

        val rewardId = RewardId(
            identifier,
            application
        )

        val saveReward = Reward(
            rewardId,
            LocalDate.now().toEpochDay(),
            "Old Title",
            "Old Description",
            0
        )

        given(rewardRepository.findOptionalById(rewardId))
            .willReturn(Optional.of(saveReward))

        val updateReward = Reward(
            rewardId,
            expectedRewardDto.expiration?.toEpochDay(),
            expectedRewardDto.title,
            expectedRewardDto.description,
            expectedRewardDto.available
        )

        given(rewardRepository.save(updateReward))
            .willReturn(updateReward)

        val result = rewardService.updateReward(identifier, expectedRewardDto, application)
        assertEquals(expectedRewardDto, result)
    }

    @Test
    fun `update a reward successfully with new available number and title`() {

        val identifier = "12345"
        val application = "application-1a"

        val expectedRewardDto = RewardDto(
            identifier,
            1,
            LocalDate.now(),
            "Test Reward",
            "Old Description"
        )

        val givenRewardDto = RewardDto(
            identifier,
            1,
            null,
            "Test Reward",
            null
        )

        val rewardId = RewardId(
            identifier,
            application
        )

        val saveReward = Reward(
            rewardId,
            expectedRewardDto.expiration?.toEpochDay(),
            "Old Title",
            "Old Description",
            0
        )

        given(rewardRepository.findOptionalById(rewardId))
            .willReturn(Optional.of(saveReward))

        val updateReward = Reward(
            rewardId,
            expectedRewardDto.expiration?.toEpochDay(),
            expectedRewardDto.title,
            expectedRewardDto.description,
            expectedRewardDto.available
        )

        given(rewardRepository.save(eq(updateReward)))
            .willReturn(updateReward)

        val result = rewardService.updateReward(identifier, givenRewardDto, application)
        assertEquals(expectedRewardDto, result)
    }

    @Test
    fun `update a reward successfully with new expiration and description`() {

        val identifier = "12345"
        val application = "application-1a"

        val expectedRewardDto = RewardDto(
            identifier,
            1,
            LocalDate.now().plusDays(1),
            "Old Title",
            "Test Description"
        )

        val givenRewardDto = RewardDto(
            identifier,
            null,
            expectedRewardDto.expiration,
            null,
            "Test Description"
        )

        val rewardId = RewardId(
            identifier,
            application
        )

        val saveReward = Reward(
            rewardId,
            LocalDate.now().toEpochDay(),
            "Old Title",
            "Old Description",
            1
        )

        given(rewardRepository.findOptionalById(rewardId))
            .willReturn(Optional.of(saveReward))

        val updateReward = Reward(
            rewardId,
            expectedRewardDto.expiration?.toEpochDay(),
            expectedRewardDto.title,
            expectedRewardDto.description,
            expectedRewardDto.available
        )

        given(rewardRepository.save(eq(updateReward)))
            .willReturn(updateReward)

        val result = rewardService.updateReward(identifier, givenRewardDto, application)
        assertEquals(expectedRewardDto, result)
    }

    @Test
    fun `update a reward successfully without attribute  values`() {

        val identifier = "23456"
        val application = "application-1a"

        val expectedRewardDto = RewardDto(
            identifier,
            null,
            null,
            null,
            null
        )

        val rewardId = RewardId(
            identifier,
            application
        )

        val saveReward = Reward(
            rewardId,
            null,
            null,
            null,
            null
        )

        given(rewardRepository.findOptionalById(rewardId))
            .willReturn(Optional.of(saveReward))

        val updateReward = Reward(
            rewardId,
            expectedRewardDto.expiration?.toEpochDay(),
            expectedRewardDto.title,
            expectedRewardDto.description,
            expectedRewardDto.available
        )

        given(rewardRepository.save(eq(updateReward)))
            .willReturn(updateReward)

        val result = rewardService.updateReward(identifier, expectedRewardDto, application)
        assertEquals(expectedRewardDto, result)
    }

    @Test
    fun `update a reward fails without finding reward by identifier and application with attribute values`() {

        val identifier = "23456"
        val application = "application-1a"

        val rewardDto = RewardDto(
            identifier,
            1,
            LocalDate.now().plusDays(1),
            "Test Title",
            null
        )

        val rewardId = RewardId(
            identifier,
            application
        )

        given(rewardRepository.findOptionalById(rewardId))
            .willReturn(Optional.empty())

        val result = rewardService.updateReward(identifier, rewardDto, application)
        assertNull(result)
    }

    @Test
    fun `delete a found reward successfully`() {

        val expectedIdentifier = "12345"
        val expectedApplication = "application-1a"

        val expectedRewardId = RewardId(
            expectedIdentifier,
            expectedApplication
        )

        val saveReward = Reward(
            expectedRewardId,
            LocalDate.now().toEpochDay(),
            "Old title",
            "Old description",
            0
        )

        given(rewardRepository.findOptionalById(expectedRewardId))
            .willReturn(Optional.of(saveReward))

        doNothing()
            .`when`(rewardRepository).deleteById(expectedRewardId)

        val identifier = rewardService.deleteReward(expectedIdentifier, expectedApplication)
        assertEquals(expectedIdentifier, identifier)
    }

    @Test
    fun `delete a not found reward successfully`() {

        val givenIdentifier = "12345"
        val givenApplication = "application-1a"
        val givenRewardId = RewardId(givenIdentifier, givenApplication)

        given(rewardRepository.findOptionalById(givenRewardId))
            .willReturn(Optional.empty())

        val identifier = rewardService.deleteReward(givenIdentifier, givenApplication)
        assertNull(identifier)
    }

    @Test
    fun `find a reward successfully with all attributes`() {

        val expectedIdentifier = "12345"
        val expectedApplication = "application-1a"

        val expectedRewardId = RewardId(
            expectedIdentifier,
            expectedApplication
        )

        val expectedRewardDto = RewardDto(
            expectedIdentifier,
            0,
            LocalDate.now(),
            "Old Title",
            "Old Description"
        )

        val saveReward = Reward(
            expectedRewardId,
            expectedRewardDto.expiration?.toEpochDay(),
            expectedRewardDto.title,
            expectedRewardDto.description,
            expectedRewardDto.available
        )

        given(rewardRepository.findOptionalById(expectedRewardId))
            .willReturn(Optional.of(saveReward))

        val rewardDto = rewardService.findRewardById(expectedIdentifier, expectedApplication)
        assertEquals(expectedRewardDto, rewardDto)
    }

    @Test
    fun `find a reward successfully without expiration`() {

        val expectedIdentifier = "12345"
        val expectedApplication = "application-1a"

        val expectedRewardId = RewardId(
            expectedIdentifier,
            expectedApplication
        )

        val expectedRewardDto = RewardDto(
            expectedIdentifier,
            0,
            null,
            "Old Title",
            "Old Description"
        )

        val saveReward = Reward(
            expectedRewardId,
            null,
            expectedRewardDto.title,
            expectedRewardDto.description,
            expectedRewardDto.available
        )

        given(rewardRepository.findOptionalById(expectedRewardId))
            .willReturn(Optional.of(saveReward))

        val rewardDto = rewardService.findRewardById(expectedIdentifier, expectedApplication)
        assertEquals(expectedRewardDto, rewardDto)
    }

    @Test
    fun `Not find a reward successfully assuming empty value`() {

        val expectedIdentifier = "12345"
        val expectedApplication = "application-1a"
        val expectedRewardId = RewardId(expectedIdentifier, expectedApplication)

        given(rewardRepository.findOptionalById(expectedRewardId))
            .willReturn(Optional.empty())

        val rewardDto = rewardService.findRewardById(expectedIdentifier, expectedApplication)
        assertNull(rewardDto)
    }

    @Test
    fun `find available rewards successfully`() {

        val application = "application-1a"

        val rewardIdOne = RewardId(
            "12345",
            application
        )

        val saveRewardOne = Reward(
            rewardIdOne,
            LocalDate.now().toEpochDay(),
            "Old title 1",
            "Old description 1",
            2
        )

        val rewardIdTwo = RewardId(
            "23456",
            application
        )

        val saveRewardTwo = Reward(
            rewardIdTwo,
            LocalDate.now().toEpochDay(),
            "Old title 2",
            "Old description 2",
            3
        )

        given(
            rewardRepository.findAllByApplicationAndExpirationAfterAndAvailableGreaterThanEqual(
                application,
                LocalDate.now().toEpochDay(),
                1L,
                PageRequest.of(0, 50)
            )
        )
            .willReturn(
                listOf(saveRewardOne, saveRewardTwo)
            )

        val results = rewardService.findAvailableRewards(application)
        assertEquals(2, results.size)

        assertEquals("12345", results[0].id)
        assertEquals("23456", results[1].id)
    }

    @Test
    fun `Not find available rewards successfully`() {

        val application = "application-1a"

        given(
            rewardRepository.findAllByApplicationAndExpirationAfterAndAvailableGreaterThanEqual(
                application,
                LocalDate.now().toEpochDay(),
                1L,
                PageRequest.of(0, 50)
            )
        )
            .willReturn(emptyList())

        val results = rewardService.findAvailableRewards(application)
        assertEquals(0, results.size)
    }

    @Test
    fun `list rewards successfully`() {

        val application = "application-1a"

        val rewardIdOne = RewardId(
            "12345",
            application
        )

        val saveRewardOne = Reward(
            rewardIdOne,
            LocalDate.now().toEpochDay(),
            "Old title 1",
            "Old description 1",
            2
        )

        val rewardIdTwo = RewardId(
            "23456",
            application
        )

        val saveRewardTwo = Reward(
            rewardIdTwo,
            null,
            "Old title 2",
            "Old description 2",
            3
        )

        given(
            rewardRepository.findAllByApplication(
                application,
                PageRequest.of(0, 50)
            )
        )
            .willReturn(
                PageImpl(
                    listOf(saveRewardOne, saveRewardTwo)
                )
            )

        val results = rewardService.listRewards(0, application)
        assertEquals(2, results.size)

        assertEquals("12345", results[0].id)
        assertEquals("23456", results[1].id)
    }

    @Test
    fun `list rewards successfully without records`() {

        val application = "application-1a"

        given(
            rewardRepository.findAllByApplication(
                application,
                PageRequest.of(0, 50)
            )
        )
            .willReturn(
                PageImpl(
                    emptyList()
                )
            )

        val results = rewardService.listRewards(0, application)
        assertEquals(0, results.size)
    }

}
