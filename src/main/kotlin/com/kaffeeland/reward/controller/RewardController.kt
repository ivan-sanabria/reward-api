/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.reward.controller

import com.kaffeeland.reward.dto.RewardDto
import com.kaffeeland.reward.service.RewardService
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

/**
 * Define the endpoint value for rewards.
 */
const val REWARD_ENDPOINT = "reward"
const val DATA_FORMAT = "application/json"
const val APPLICATION_HEADER = "application"

/**
 * Class responsible of handling operations with client rewards.
 *
 * @author  Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since   1.0.0
 */
@RestController
class RewardController @Autowired constructor(private val rewardService: RewardService) {

    /**
     * Method to create rewards into the system.
     *
     * @param application   Application owner of the reward to create.
     * @param rewardDto     Reward data transfer object that encapsulates the information of reward domain to insert on the system.
     * @return              Response entity with status code.
     */
    @ApiOperation("Create a reward in the system with the given information.")
    @ApiResponses(
        value = [
            ApiResponse(code = 201, message = "Creation of the reward in the system was executed successfully."),
            ApiResponse(code = 400, message = "The given json is not parsable to a reward reference."),
            ApiResponse(
                code = 415,
                message = "Given content-type on the request was not correct. Expected is application/json."
            ),
            ApiResponse(code = 500, message = "Server error occurs by processing the post request.")]
    )
    @PostMapping(REWARD_ENDPOINT, produces = [DATA_FORMAT])
    @ResponseStatus(HttpStatus.CREATED)
    fun createReward(
        @RequestHeader(value = APPLICATION_HEADER) application: String,
        @RequestBody rewardDto: RewardDto
    ): ResponseEntity<Void> {

        val identifier = rewardService.createReward(rewardDto, application)

        return if (identifier.isEmpty())
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        else
            ResponseEntity(HttpStatus.CREATED)
    }

    /**
     * Method to update rewards into the system.
     *
     * @param application   Application owner of the reward to update.
     * @param id            Identifier of the reward to update on the data layer.
     * @param rewardDto     Reward data transfer object with the information to update on the data layer.
     * @return              Response entity with status code.
     */
    @ApiOperation("Update a reward in the system with the given identifier.")
    @ApiResponses(
        value = [
            ApiResponse(code = 200, message = "Update of the reward in the system was executed successfully."),
            ApiResponse(code = 400, message = "The given json is not parsable to a reward reference."),
            ApiResponse(
                code = 415,
                message = "Given content-type on the request was not correct. Expected is application/json."
            ),
            ApiResponse(code = 500, message = "Server error occurs by processing the put request.")]
    )
    @PutMapping("$REWARD_ENDPOINT/{id}", produces = [DATA_FORMAT])
    fun updateReward(
        @RequestHeader(value = APPLICATION_HEADER) application: String, @PathVariable(value = "id") id: String,
        @RequestBody rewardDto: RewardDto
    ): ResponseEntity<Void> {

        val updateRewardDto = rewardService.updateReward(id, rewardDto, application)

        return if (null == updateRewardDto)
            ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        else
            ResponseEntity(HttpStatus.OK)
    }

    /**
     * Method to delete rewards into the system.
     *
     * @param application   Application owner of the reward to delete.
     * @param id            Identifier of the reward to delete on the data layer.
     * @return              Response entity with status code.
     */
    @ApiOperation("Delete a reward in the system with the given identifier.")
    @ApiResponses(
        value = [
            ApiResponse(code = 200, message = "Delete of the reward in the system was executed successfully."),
            ApiResponse(code = 404, message = "Reward with the given identifier and application was not found"),
            ApiResponse(code = 500, message = "Server error occurs by processing the delete request.")]
    )
    @DeleteMapping("$REWARD_ENDPOINT/{id}", produces = [DATA_FORMAT])
    fun deleteReward(
        @RequestHeader(value = APPLICATION_HEADER) application: String,
        @PathVariable(value = "id") id: String
    ): ResponseEntity<Void> {

        val identifier = rewardService.deleteReward(id, application)

        return if (null == identifier)
            ResponseEntity(HttpStatus.NOT_FOUND)
        else
            ResponseEntity(HttpStatus.OK)
    }

    /**
     * Method to request rewards by identifier.
     *
     * @param application   Application owner of the reward to request to data layer.
     * @param id            Identifier of the reward to request to data layer.
     * @return              Response entity with status code and reward response body.
     */
    @ApiOperation("Retrieves specific reward with the given identifier.")
    @ApiResponses(
        value = [
            ApiResponse(code = 200, message = "Reward was found successfully."),
            ApiResponse(code = 404, message = "A reward with the given identifier and application was not found."),
            ApiResponse(code = 500, message = "Server error occurs by processing the get request.")]
    )
    @GetMapping("$REWARD_ENDPOINT/{id}", produces = [DATA_FORMAT])
    fun getRewardById(
        @RequestHeader(value = APPLICATION_HEADER) application: String,
        @PathVariable(value = "id") id: String
    ): ResponseEntity<RewardDto> {

        val rewardDto = rewardService.findRewardById(id, application)

        return if (null == rewardDto)
            ResponseEntity(HttpStatus.NOT_FOUND)
        else
            ResponseEntity.ok(rewardDto)
    }

    /**
     * Method to request all rewards stored in the system.
     *
     * @param application   Application owner of the rewards to request to data layer.
     * @param page          Page number to get specific number of rewards.
     * @return              Response entity with status code and reward list response body.
     */
    @ApiOperation("Retrieves stored rewards in the system.")
    @ApiResponses(
        value = [
            ApiResponse(code = 200, message = "Rewards were listed successfully."),
            ApiResponse(code = 500, message = "Server error occurs by processing the get request.")]
    )
    @GetMapping(REWARD_ENDPOINT, produces = [DATA_FORMAT])
    fun getRewards(
        @RequestHeader(value = APPLICATION_HEADER) application: String,
        @RequestParam(required = false, value = "page", defaultValue = "0") page: Long
    ): ResponseEntity<List<RewardDto>> {

        val rewardDtoList = rewardService.listRewards(page, application)

        return ResponseEntity.ok(rewardDtoList)
    }

    /**
     * Method to request available rewards.
     *
     * @param application   Application owner of the available rewards to request to data layer.
     * @return              Response entity with status code and reward list response body.
     */
    @ApiOperation("Retrieves available rewards in the system.")
    @ApiResponses(
        value = [
            ApiResponse(code = 200, message = "Available rewards were listed successfully."),
            ApiResponse(code = 500, message = "Server error occurs by processing the get request.")]
    )
    @GetMapping("$REWARD_ENDPOINT/available", produces = [DATA_FORMAT])
    fun getAvailableRewards(@RequestHeader(value = APPLICATION_HEADER) application: String): ResponseEntity<List<RewardDto>> {

        val rewardDtoList = rewardService.findAvailableRewards(application)

        return ResponseEntity.ok(rewardDtoList)
    }

}
