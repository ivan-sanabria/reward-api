/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.reward.dto

import io.swagger.annotations.ApiModelProperty
import java.time.LocalDate

/**
 * Reward data transfer object used as a contract with the client applications.
 *
 * @author  Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since   1.0.0
 *
 * @property id             Unique identifier of the reward.
 * @property available      How many users could win the reward.
 * @property expiration     Expiration date of the reward.
 * @property title          Title of the reward.
 * @property description    Description of the reward.
 */
data class RewardDto(

    @ApiModelProperty(notes = "Unique identifier of reward.", example = "698d1f68-9322-4664-854b-c4ae610c6952")
    var id: String?,

    @ApiModelProperty(notes = "Quantity of rewards available for users.", example = "3")
    var available: Long?,

    @ApiModelProperty(notes = "Until when the reward is available.", example = "2019-12-31")
    var expiration: LocalDate?,

    @ApiModelProperty(notes = "Title of the reward.", example = "Travel to coffee hacienda")
    var title: String?,

    @ApiModelProperty(
        notes = "Description of the reward.",
        example = "Get 2 to travel packages to coffee hacienda everything included."
    )
    var description: String?
)
