/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.reward.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

/**
 * Class to configure swagger api documentation for clients.
 *
 * @author  Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since   1.0.0
 */
@Configuration
@EnableSwagger2
class SwaggerConfiguration {

    /**
     * Bean used to configure swagger to specify supported operations, protocols, formats of reward api.
     */
    @Bean
    fun api(): Docket = Docket(DocumentationType.SWAGGER_2)
        .groupName("Kaffeeland User Services")
        .consumes(setOf("application/json"))
        .protocols(setOf("http"))
        .useDefaultResponseMessages(false)
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.kaffeeland.reward.controller"))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(apiEndPointsInfo())

    /**
     * Define general information of reward api.
     */
    private fun apiEndPointsInfo(): ApiInfo = ApiInfoBuilder()
        .title("Kaffeeland Reward Rest Api")
        .description("Service responsible of handle rewards for best users of kaffeeland services.")
        .contact(
            Contact(
                "Iván Camilo Sanabria",
                "https://www.linkedin.com/in/icsanabriar/en",
                "icsanabriar@googlemail.com"
            )
        )
        .license("Apache 2.0")
        .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
        .version("1.0.0")
        .build()

}
