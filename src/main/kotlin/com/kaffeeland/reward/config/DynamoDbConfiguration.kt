/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.reward.config

import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Class to configure dynamo db client to consume data from database.
 *
 * @author  Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since   1.0.0
 */
@Configuration
@EnableDynamoDBRepositories(basePackages = ["com.kaffeeland.reward.repository"])
class DynamoDBConfig {

    /**
     * Define the bean to create connection to dynamo db.
     */
    @Bean
    fun amazonDynamoDB(): AmazonDynamoDB {

        val credentials = EnvironmentVariableCredentialsProvider()

        return AmazonDynamoDBClientBuilder.standard()
            .withCredentials(credentials)
            .build()
    }

}
