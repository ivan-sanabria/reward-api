/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.reward.repository

import com.kaffeeland.reward.domain.Reward
import com.kaffeeland.reward.domain.RewardId
import org.socialsignin.spring.data.dynamodb.repository.EnableScan
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.Optional

/**
 * Repository responsible of defining the operations to persist domain object into DynamoDB.
 *
 * @author  Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since   1.0.0
 */
@EnableScan
@EnableScanCount
interface RewardRepository : PagingAndSortingRepository<Reward, RewardId> {

    /**
     * Find reward by identifier and application.
     *
     * @param rewardId  Reward identifier compose by unique identifier and application.
     * @return          Optional reward identifier with the given unique identifier and application.
     */
    fun findOptionalById(
        rewardId: RewardId
    ): Optional<Reward>

    /**
     * Find all rewards by application.
     *
     * @param application   Application to filter the reward search.
     * @param page          Page to filter the number of reward search.
     * @return              Page with the found rewards.
     */
    fun findAllByApplication(
        application: String,
        page: Pageable
    ): Page<Reward>

    /**
     * Find available rewards now by application.
     *
     * @param application   Application to filter the reward search.
     * @param expiration    Expiration to filter the reward search.
     * @param available     Available to filter the reward search.
     * @param page          Page to filter the number of reward search.
     * @return              List with the found rewards.
     */
    fun findAllByApplicationAndExpirationAfterAndAvailableGreaterThanEqual(
        application: String,
        expiration: Long,
        available: Long,
        page: Pageable
    ): List<Reward>

}
