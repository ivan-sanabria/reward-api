/*
 * Copyright (C) 2019 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kaffeeland.reward.service

import com.kaffeeland.reward.domain.Reward
import com.kaffeeland.reward.domain.RewardId
import com.kaffeeland.reward.dto.RewardDto
import com.kaffeeland.reward.repository.RewardRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.util.LinkedList

/**
 * Service responsible of defining the operations offered around reward domain.
 *
 * @author  Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since   1.0.0
 */
@Service
class RewardService @Autowired constructor(private val rewardRepository: RewardRepository) {

    /**
     * Creates a new reward in the system.
     *
     * @param rewardDto     Reward data transfer object with the information to create new reward.
     * @param application   Application identifier owner of the reward.
     * @return              Unique identifier of the created reward.
     */
    fun createReward(rewardDto: RewardDto, application: String): String {

        val rewardId = RewardId(null, application)
        val expiration = rewardDto.expiration?.toEpochDay()

        val reward = Reward(
            rewardId,
            expiration,
            rewardDto.title,
            rewardDto.description,
            rewardDto.available
        )

        return rewardRepository.save(reward).id.uuid!!
    }

    /**
     * Updates an existing reward in the system.
     *
     * @param id            Identifier of the reward to update information.
     * @param rewardDto     Reward data transfer object with the updated information of specific reward.
     * @param application   Application identifier owner of the reward.
     * @return              Reward data transfer object with the updated data.
     */
    fun updateReward(id: String, rewardDto: RewardDto, application: String): RewardDto? {

        val rewardId = RewardId(id, application)
        val optionalReward = rewardRepository.findOptionalById(rewardId)

        return if (optionalReward.isPresent) {

            val originReward = optionalReward.get()

            val expiration = rewardDto.expiration?.toEpochDay() ?: originReward.expiration
            val title = rewardDto.title ?: originReward.title
            val description = rewardDto.description ?: originReward.description
            val available = rewardDto.available ?: originReward.available

            val reward = Reward(rewardId, expiration, title, description, available)
            val updated = rewardRepository.save(reward)

            var updatedExpiration: LocalDate? = null

            if (null != updated.expiration)
                updatedExpiration = LocalDate.ofEpochDay(updated.expiration!!)

            RewardDto(
                updated.id.uuid,
                updated.available,
                updatedExpiration,
                updated.title,
                updated.description
            )

        } else null

    }

    /**
     * Deletes an existing reward of the system.
     *
     * @param id            Identifier of the reward to delete from the system.
     * @param application   Application identifier owner of the reward.
     * @return              Unique identifier of the deleted reward.
     */
    fun deleteReward(id: String, application: String): String? {

        val rewardId = RewardId(id, application)
        val optionalReward = rewardRepository.findOptionalById(rewardId)

        return if (optionalReward.isPresent) {

            rewardRepository.deleteById(rewardId)

            optionalReward.get().id.uuid

        } else null

    }

    /**
     * Find an specific reward using the identifier.
     *
     * @param id            Identifier of the reward to make the search.
     * @param application   Application identifier owner of the reward.
     * @return              Reward data transfer object with the found data.
     */
    fun findRewardById(id: String, application: String): RewardDto? {

        val rewardId = RewardId(id, application)
        val optionalReward = rewardRepository.findOptionalById(rewardId)

        return if (optionalReward.isPresent) {

            val reward = optionalReward.get()

            var expiration: LocalDate? = null

            if (null != reward.expiration)
                expiration = LocalDate.ofEpochDay(reward.expiration!!)

            RewardDto(
                reward.id.uuid,
                reward.available,
                expiration,
                reward.title,
                reward.description
            )

        } else null

    }

    /**
     * Find all the rewards that are available now.
     *
     * @param application   Application identifier owner of the rewards.
     * @return              List of reward data transfer object with the found data.
     */
    fun findAvailableRewards(application: String): List<RewardDto> {

        val availableFilter = 1L

        val expirationFilter = LocalDate.now()
            .toEpochDay()

        val pageable = PageRequest.of(0, 50)

        val rewards = rewardRepository.findAllByApplicationAndExpirationAfterAndAvailableGreaterThanEqual(
            application,
            expirationFilter,
            availableFilter,
            pageable
        )

        val results = LinkedList<RewardDto>()

        for (reward in rewards) {

            val expiration = LocalDate.ofEpochDay(reward.expiration!!)

            val rewardDto = RewardDto(
                reward.id.uuid,
                reward.available!!,
                expiration,
                reward.title,
                reward.description
            )

            results.add(rewardDto)
        }

        return results
    }

    /**
     * List all the rewards that exists on the system.
     *
     * @param page          Page to filter the search.
     * @param application   Application identifier owner of the rewards.
     * @return              List of reward data transfer object with the found data.
     */
    fun listRewards(page: Long, application: String): List<RewardDto> {

        val pageable = PageRequest.of(page.toInt(), 50)
        val rewards = rewardRepository.findAllByApplication(application, pageable)

        val results = LinkedList<RewardDto>()

        for (reward in rewards) {

            var expiration: LocalDate? = null

            if (null != reward.expiration)
                expiration = LocalDate.ofEpochDay(reward.expiration!!)

            val rewardDto = RewardDto(
                reward.id.uuid,
                reward.available!!,
                expiration,
                reward.title,
                reward.description
            )

            results.add(rewardDto)
        }

        return results
    }

}
