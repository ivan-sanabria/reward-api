#!/usr/bin/env bash

# Define variables to have standard name on deployment
export S3_BUCKET="kaffeeland-apis"
export CNAME_PREFIX="kaffeeland-reward-api"

export APPLICATION="${BITBUCKET_REPO_SLUG}"
export VERSION="${BITBUCKET_BUILD_NUMBER}"
export ENVIRONMENT=${ENVIROMENT_NAME:-"reward-api-prod"}

export S3_KEY="kaffeeland-reward-api-${VERSION}.jar"

# Execute S3 and elastic beanstalk to create application version and environment for deployment
aws s3 cp build/libs/kaffeeland-reward-api-*.jar s3://kaffeeland-apis/"${S3_KEY}"
aws elasticbeanstalk create-application-version --application-name "${APPLICATION}" --version-label "${VERSION}" --auto-create-application --source-bundle S3Bucket="${S3_BUCKET}",S3Key="${S3_KEY}"
aws elasticbeanstalk create-environment --application-name "${APPLICATION}" --environment-name "${ENVIRONMENT}" --cname-prefix "${CNAME_PREFIX}" --version-label "${VERSION}" --solution-stack-name "64bit Amazon Linux 2018.03 v2.8.6 running Java 8" --option-settings file://options.txt
