#!/usr/bin/env bash

# Define variables to have standard name on deployment
export S3_BUCKET="kaffeeland-apis"
export CNAME_PREFIX="kaffeeland-reward-api"

export APPLICATION="${BITBUCKET_REPO_SLUG}"
export VERSION="${BITBUCKET_BUILD_NUMBER}"
export ENVIRONMENT=${ENVIROMENT_NAME:-"reward-api-prod"}

export S3_KEY="kaffeeland-reward-api-${VERSION}.jar"

# Execute S3 and elastic beanstalk to create application version and update environment for deployment
aws s3 cp build/libs/kaffeeland-reward-api-*.jar s3://kaffeeland-apis/"${S3_KEY}"
aws elasticbeanstalk create-application-version --application-name "${APPLICATION}" --version-label "${VERSION}" --auto-create-application --source-bundle S3Bucket="${S3_BUCKET}",S3Key="${S3_KEY}"
aws elasticbeanstalk update-environment --application-name "${APPLICATION}" --environment-name "${ENVIRONMENT}" --version-label "${VERSION}" --option-settings '[{"Namespace":"aws:elasticbeanstalk:application:environment","OptionName":"AWS_ACCESS_KEY_ID","Value":"'"${AWS_ACCESS_KEY_ID}"'"},{"Namespace":"aws:elasticbeanstalk:application:environment","OptionName":"AWS_SECRET_KEY","Value":"'"${AWS_SECRET_ACCESS_KEY}"'"}]'
